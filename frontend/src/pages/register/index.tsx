import React, {useState} from "react";
import SignInUpForm from "../../components/SignInUpForm";
import {useRegisterMutation} from "../../generated/types";
import {useNavigate} from "react-router-dom";

const Register = () => {
    const navigate = useNavigate()
    const onSubmit = (data: any, error: any) => {
            return navigate('/login')
    }
    // @ts-ignore
    return <SignInUpForm onSubmit={onSubmit} validationError="User already exist" title='Register' action={useRegisterMutation} />
}

export default Register;