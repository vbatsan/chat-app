import React, {useState} from "react";
import SignInUpForm from "../../components/SignInUpForm";
import {LoginMutation, useLoginMutation} from "../../generated/types";
import {useAppDispatch} from "../../hooks/store";
import {update} from "../../store/user";

const Login = () => {
    const dispatch = useAppDispatch()
    const onSubmit = (data: LoginMutation) => {
            const user = {
                ...data.login,
                isAuthenticated: true,
            }
            localStorage.setItem('token', data.login.token)
            dispatch(update(user))
    }

    return (
        // @ts-ignore
        <SignInUpForm onSubmit={onSubmit} validationError="Wrong credentials" title='Login' action={useLoginMutation}/>
    )
}

export default Login