import React from 'react'
import Grid2 from "@mui/material/Unstable_Grid2";
import {useAppSelector} from "../../hooks/store";
import {Typography} from "@mui/material";

const ChatHeader = () => {
    const {activeRoom} = useAppSelector(state => state.rooms)
    return (
        <Grid2
            sx={{
                backgroundColor: '#edede9',
                boxShadow: '-2px 10px 14px -12px rgba(0,0,0,0.75)',
                padding: '20px 5px',
            }}
            container
            justifyContent="center"
        >
            <Grid2>
                <Typography component='h6'>{activeRoom?.name}</Typography>
            </Grid2>
        </Grid2>
    )
}

export default ChatHeader