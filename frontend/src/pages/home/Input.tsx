import React, {ChangeEvent, useState} from 'react'
import {useAppDispatch, useAppSelector} from "../../hooks/store";
import Grid2 from "@mui/material/Unstable_Grid2";
import {Button, FormControl, TextField} from "@mui/material";
import SendIcon from '@mui/icons-material/Send';
import {useSendMessageMutation} from "../../generated/types";
import {internalUpdate} from "../../store/messages";

const Input = () => {
    const [message, setMessage] = useState('')
    const dispatch = useAppDispatch()
    const {activeRoom} = useAppSelector(state => state.rooms)
    const [sendMessage, {data}] = useSendMessageMutation({
        variables: {
            // @ts-ignore
            roomId: activeRoom.id,
            message: {text: message}
        }
    })

    const onMessageSend = (event: React.FormEvent) => {
        event.preventDefault()
        sendMessage()
            .finally(() => setMessage(''))

    }

    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setMessage(event.target.value)
    }

    return (
        <form onSubmit={onMessageSend}>
            <Grid2 container width='100%' alignItems='center' spacing={1} sx={{padding: '10px'}}>
                <Grid2 xs={10}>
                    <TextField
                        fullWidth
                        id="message"
                        placeholder='Type message'
                        color='primary'
                        value={message}
                        variant="standard"
                        onChange={onChange}
                    />
                </Grid2>
                <Grid2 xs={2}>
                    <Button type="submit">
                        <SendIcon/>
                    </Button>
                </Grid2>
            </Grid2>
        </form>
    )
}

export default Input;