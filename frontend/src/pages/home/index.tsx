import React from 'react';
import Rooms from "./Rooms";
import {useAppDispatch, useAppSelector} from "../../hooks/store";
import Grid2 from "@mui/material/Unstable_Grid2";
import Chat from "./Chat";
import {useGetRoomsQuery, useSubscribeForRoomsSubscription} from "../../generated/types";
import {update} from "../../store/rooms";
import RoomsEmptyState from "./RoomsEmptyState";

const Home = React.memo(() => {
    const {list: rooms} = useAppSelector(state => state.rooms)
    const {user} = useAppSelector(state => state.user)
    const dispatch = useAppDispatch()
    useGetRoomsQuery({
        // @ts-ignore
        variables: {userId: user.id},
        // @ts-ignore
        onCompleted: (data) => dispatch(update(data.rooms))
    })

    useSubscribeForRoomsSubscription({
        variables: {
            // @ts-ignore
            userId: user.id
        },
        onSubscriptionData: ({subscriptionData}) => {
            // @ts-ignore
            dispatch(update(subscriptionData?.data?.rooms))
        }
    })
    if (!rooms.length) {
        return <RoomsEmptyState/>
    }
    return (
        <Grid2 container height='100%'>
            <Grid2 xs={3}><Rooms rooms={rooms} /></Grid2>
            <Grid2 height='100vh' xs={9}><Chat/></Grid2>
        </Grid2>
    )
})
export default Home;