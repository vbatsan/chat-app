import React from 'react'
import {useAppSelector} from "../../hooks/store";
import Grid2 from "@mui/material/Unstable_Grid2";
import {Typography} from "@mui/material";
import ChatHeader from "./ChatHeader";
import Input from "./Input";
import Messages from "./Messages";

const Chat = () => {
    const {activeRoom} = useAppSelector(state => state.rooms)

    if (!activeRoom) {
        return (
            <Grid2 container justifyContent='center' alignItems='center' height='100%'>
                <Grid2>
                    <Typography>Select chat to start messaging</Typography>
                </Grid2>
            </Grid2>
        )
    }
    return (
        <Grid2 container height='100%' direction='column'>
            <Grid2> <ChatHeader /></Grid2>
            <Grid2 sx={{overflowY: 'scroll'}} flex={15}><Messages/></Grid2>
            <Grid2 flex={1}><Input /></Grid2>
        </Grid2>
    )
}

export default Chat;