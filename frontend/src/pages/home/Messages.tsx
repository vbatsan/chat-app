import React, {FC, useEffect} from 'react'
import {IMessage, update} from "../../store/messages";
import {Box, Typography} from "@mui/material";
import {useAppSelector} from "../../hooks/store";
import {useGetRoomMessagesQuery, useSubscribeForMessagesSubscription} from "../../generated/types";
import {useDispatch} from "react-redux";
import Message from "./Message";


const Messages = () => {
    const {activeRoom} = useAppSelector(state => state.rooms)
    const dispatch = useDispatch()
    // @ts-ignore
    const messages = useAppSelector(state => state.messages[activeRoom.id] || [])

    useGetRoomMessagesQuery({
        // @ts-ignore
        variables: {roomId: activeRoom.id},
        onCompleted: (data) => {
            // @ts-ignore
            dispatch(update({id: activeRoom.id, messages: data.messages}))
        }
    })

    useSubscribeForMessagesSubscription({
        variables:{
            // @ts-ignore
            roomId: activeRoom?.id
        },
        onSubscriptionData: ({subscriptionData}) => {
            // @ts-ignore
            dispatch(update({id: activeRoom?.id, messages: subscriptionData.data?.message}))
        }
    })

    return(
        <Box>
            {messages.map(message => <Message key={message.id} message={message} />)}
        </Box>
    )
}

export default Messages