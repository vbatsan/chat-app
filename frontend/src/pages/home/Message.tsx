import React, {FC} from 'react'
import {IMessage} from "../../store/messages";
import Grid2 from "@mui/material/Unstable_Grid2";
import {Box, Typography} from "@mui/material";
import {useAppSelector} from "../../hooks/store";
import moment from "moment";

interface IProps{
    message: IMessage
}

const Message:FC<IProps> = ({message}) => {
    const {user} = useAppSelector(state => state.user)

    const isMyMessage = message?.user?.id === user?.id
    const messageBackground = isMyMessage ? '#84a98c' : '#cad2c5'
    return(
        <Grid2 container justifyContent={isMyMessage ? 'flex-end' : 'flex-start'}>
            <Grid2 sx={{padding: '10px'}}>
                <Box sx={{
                    padding: '5px',
                    backgroundColor: messageBackground,
                    minWidth: '150px',
                    maxWidth: '300px',
                    borderRadius: '5px'
                }}>
                    <Typography color="white" component={'h4'}>{message?.text}</Typography>
                    <Grid2 container justifyContent="flex-end">
                        <Grid2>
                            <Typography color='mintcream' fontSize='12px' component='p'>{moment(message?.date).format('YYYY-MM-DD-HH:MM:ss')}</Typography>
                        </Grid2>
                    </Grid2>
                </Box>
            </Grid2>
        </Grid2>
    )
}

export default Message