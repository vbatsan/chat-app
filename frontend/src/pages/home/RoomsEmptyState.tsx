import React, {useState} from 'react';
import Grid2 from "@mui/material/Unstable_Grid2";
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import {useAppDispatch} from "../../hooks/store";
import {openUsers} from "../../store/modals";

const RoomsEmptyState = () => {
   const dispatch = useAppDispatch()
    const openUserModal = () => dispatch(openUsers())

    return(
        <Grid2 container height="100vh" justifyContent="center" alignItems="center">
            <Grid2>
                <Button onClick={openUserModal} startIcon={<AddIcon/>}>Add user</Button>
            </Grid2>
        </Grid2>
    )
}

export default RoomsEmptyState;


