import React, {FC} from 'react'
import {Button, List, ListItem, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import Logo from "../../components/Avatar";
import {useAppDispatch, useAppSelector} from "../../hooks/store";
import {IRoom, setActiveRoom, update} from "../../store/rooms";
import {openUsers} from "../../store/modals";

const RoomItem: FC<IRoom> = ({name, id}) => {
    const dispatch = useAppDispatch()
    const {user} = useAppSelector(state => state.user)
    const handleClick = () => dispatch(setActiveRoom({name, id}))
    // @ts-ignore
    const formattedName = name.replace(user?.name, '')
    return (
        <ListItem disablePadding>
            <ListItemButton onClick={handleClick}>
                <ListItemIcon>
                    <Logo name={formattedName}/>
                </ListItemIcon>
                <ListItemText primary={formattedName}/>
            </ListItemButton>
        </ListItem>
    )
}

interface IProps{
    rooms: IRoom[]
}

const Rooms:FC<IProps> = ({rooms}) => {
    const dispatch = useAppDispatch()

    const openUserModal = () => dispatch(openUsers())

    return (
        <List sx={{
            height: '100vh',
            backgroundColor: '#f8edeb',
            position: 'relative'
        }}>
            {rooms.map((room) => <RoomItem name={room?.name} id={room?.id} key={room?.id}/>)}
            <ListItem sx={{position: 'absolute', bottom: 10, left: 0, transform: 'translate( 15%, -10px)'}}>
                <Button onClick={openUserModal} startIcon={<AddIcon/>}>
                    Add User
                </Button>
            </ListItem>
        </List>
    )
}

export default Rooms;