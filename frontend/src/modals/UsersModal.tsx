import React, {FC} from 'react';
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import {blue} from "@mui/material/colors";
import PersonIcon from "@mui/icons-material/Person";
import ListItemText from "@mui/material/ListItemText";
import {useCreateRoomMutation, useGetUsersQuery} from "../generated/types";
import {CircularProgress} from "@mui/material";
import {useAppDispatch, useAppSelector} from "../hooks/store";
import {closeUsers} from "../store/modals";
import {addToRooms} from "../store/rooms";

const UsersModal = () => {
    const {data, loading} = useGetUsersQuery()
    // @ts-ignore
    const {user: {id}} = useAppSelector(state => state.user)
    const {users: usersModal} = useAppSelector(state => state.modals)
    const rooms = useAppSelector(state => state.rooms.list)
    const existingRooms = rooms.map(room => room.name)
    const users = data?.users?.filter(user => user.id !== id && !existingRooms.includes(user.name)) || []
    const dispatch = useAppDispatch()

    const closeModal = () => dispatch(closeUsers())

    const [createRoom, { error, data: roomData}] = useCreateRoomMutation()

    const handleClick = (id: number) => {
        createRoom({variables: {userId: id}})
            .then(closeModal)
    }

    return (
        <Dialog onClose={closeModal} open={usersModal}>
            <DialogTitle>Start chat with user</DialogTitle>
            {loading && <CircularProgress />}
            {data && (
                <List sx={{ pt: 0 }}>
                    {users.map(({name, id}) => (
                        <ListItem button onClick={() => handleClick(id)} key={name}>
                            <ListItemAvatar>
                                <Avatar sx={{ bgcolor: blue[100], color: blue[600] }}>
                                    <PersonIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={name} />
                        </ListItem>
                    ))}
                </List>
            )}
        </Dialog>
    );
}

export default UsersModal