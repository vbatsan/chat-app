import { gql } from '@apollo/client';
export const REGISTER = gql`
    mutation($login: String!, $password: String!){
        register(name: $login, password: $password){
            name
            id
        }
    }
`