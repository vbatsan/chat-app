import React, {FC} from "react";
import {useFormik} from "formik";
import Grid2 from "@mui/material/Unstable_Grid2";
import {Button, TextField, Typography} from "@mui/material";
import {useRegisterMutation, useLoginMutation, LoginMutation, RegisterMutation} from "../generated/types";
import {log} from "util";
import {Link} from "react-router-dom";

type TMutation = typeof useRegisterMutation | typeof useLoginMutation

interface ISignFormInitial {
    login: string
    password: string
}

type TTitle = 'Register' | 'Login'

interface ISignForm {
    title: TTitle
    action: TMutation
    onSubmit: (data: LoginMutation | RegisterMutation) => void,
    validationError?: string
}

const secondaryLink = {
    Login: {
        title: 'Registration',
        path: '/register',
    },
    Register: {
        title: 'Login',
        path: '/login'
    }
}

const SignInUpForm: FC<ISignForm> = ({title, action, onSubmit, validationError}) => {
    const {handleSubmit, handleChange, values} = useFormik<ISignFormInitial>({
        initialValues: {
            login: '',
            password: '',
        },
        onSubmit: () => submit()
    })
    const [submit, {data, loading, error}] = action({variables: values, onCompleted: onSubmit})
    return (
        <Grid2 direction="column" justifyContent='center' alignItems='center' gap='15px' container height='100vh'>
            <Typography component='h1' color="blue">{title}</Typography>
            <Grid2>
                <TextField
                    error={!!error}
                    helperText={error ? validationError : ''}
                    name="login"
                    id="login"
                    label="Login"
                    variant="outlined"
                    onChange={handleChange}
                    value={values.login}
                />
            </Grid2>
            <Grid2>
                <TextField
                    name="password"
                    id="password"
                    label="Password"
                    variant="outlined"
                    type="password"
                    onChange={handleChange}
                    value={values.password}
                />
            </Grid2>
            <Grid2 container>
                <Grid2><Button onClick={() => handleSubmit()} variant="contained">Submit</Button></Grid2>
                <Grid2>
                    <Link to={secondaryLink[title].path}>
                        <Button>{secondaryLink[title].title}</Button>
                    </Link>
                </Grid2>
            </Grid2>
        </Grid2>
    )
}

export default SignInUpForm;
