import React, {FC} from 'react';
import {Avatar as Logo} from "@mui/material";
import {randomColor} from "../utils";

interface IProps {
    name: string
    color?: string
}

const Avatar: FC<IProps> = React.memo(({name, color }) => {
    const background = color ?? randomColor()
    return(
        <Logo sx={{backgroundColor: background}}>{name.substring(0,2).toUpperCase()}</Logo>
    )
})

export default Avatar;
