import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'


export interface IUser {
    id: number
    name: string
}

interface TInitialState {
    user: IUser | null
    token: string | null
    isAuthenticated: boolean
}

const initialState: TInitialState = {
    user: null,
    token: localStorage.getItem('token') || null,
    isAuthenticated: false
}


export const counterSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        update: (state, action: { payload: any }) => action.payload,
    },
})

export const { update } = counterSlice.actions

export default counterSlice.reducer