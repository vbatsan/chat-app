import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export interface IRoom {
    name: string
    id: number
}

interface IState {
    list: IRoom[],
    activeRoom: IRoom | null
}

const initialState:IState = {
    list: [],
    activeRoom: null,
}

const roomsSlice = createSlice({
    name: 'rooms',
    initialState,
    reducers: {
        update: (state, action:PayloadAction<IRoom[]>) => {
            state.list = action.payload
            return state
        },
        addToRooms: (state, action:PayloadAction<IRoom>) => {
            state.list.push(action.payload)
            return state
        },
        setActiveRoom: (state, action: PayloadAction<IRoom>) => {
            state.activeRoom = action.payload
            return state
        }
    }
})

export const {update, addToRooms, setActiveRoom} = roomsSlice.actions
export default roomsSlice.reducer