import {createSlice} from "@reduxjs/toolkit";

interface IState{
    [key: string]: boolean
}

const initialState:IState = {
    users: false,
}

const modalSlice = createSlice({
    name: 'modals',
    initialState,
    reducers: {
        openUsers: state => {
            state.users = true
            return state
        },
        closeUsers: state => {
            state.users = false
            return state
        },
    }
})

export const {openUsers, closeUsers} = modalSlice.actions;
export default modalSlice.reducer;