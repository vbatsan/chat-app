import user from './user'
import rooms from './rooms'
import modals from './modals'
import messages from './messages'

export default {
    user,
    rooms,
    modals,
    messages,
}