import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IUser} from "../user";

export interface IMessage{
    id: number
    user: IUser
    text: string
    date: string
}

export interface IUpdateMessagesPayload{
    id: number
    messages: IMessage[]
}

export interface IUpdateMessageInternal{
    id: number
    message: IMessage
}

interface IState{
    [key:number]: IMessage[]
}

const initialState: IState = {}

const messageSlice = createSlice({
    name: 'message',
    initialState,
    reducers: {
        update: (state, action:PayloadAction<IUpdateMessagesPayload>) => {
            state[action.payload.id] = action.payload.messages
            return state
        },
        internalUpdate: (state, action:PayloadAction<IUpdateMessageInternal>) => {
            state[action.payload.id].push(action.payload.message)
            return state
        }
    }
})

export const {update, internalUpdate} = messageSlice.actions
export default messageSlice.reducer