import React, {useEffect} from 'react';
import {ApolloClient, InMemoryCache, ApolloProvider, createHttpLink, split, DefaultOptions} from '@apollo/client';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { createClient } from 'graphql-ws';

import './App.css';
import Login from "./pages/login";
import { Route, Routes, useNavigate } from "react-router-dom";
import Register from "./pages/register";
import {useAppSelector} from "./hooks/store";
import {setContext} from "@apollo/client/link/context";
import Home from "./pages/home";
import {getMainDefinition} from "@apollo/client/utilities";
import UsersModal from "./modals/UsersModal";

const wsLink = new GraphQLWsLink(createClient({
    url: 'ws://localhost:4000/subscriptions',
}));

const httpLink = createHttpLink({
    uri: 'http://localhost:4000/graphql',
});

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('token');
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : "",
        }
    }
});

const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink,
    authLink.concat(httpLink),
);

const defaultOptions: DefaultOptions = {
    watchQuery: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'ignore',
    },
    query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
    },
}
const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache({}),
    defaultOptions,
});

function App() {
    const {isAuthenticated} = useAppSelector(state => state.user)
    const navigate = useNavigate()

    useEffect(() => {
        if (isAuthenticated) {
            return navigate('/')
        }
        navigate('/login')
    }, [isAuthenticated])

    return (
        <ApolloProvider client={client}>
            <Routes>
                <Route path='/login' element={<Login/>}/>
                <Route path='/register' element={<Register/>}/>
                {isAuthenticated &&  <Route path="/" element={<Home />} />}
            </Routes>
            {isAuthenticated && (
                <UsersModal />
            )}
        </ApolloProvider>
    );
}

export default App;
