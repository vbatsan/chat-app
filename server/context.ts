import { PrismaClient } from '@prisma/client'
import { createPubSub, PubSub } from '@graphql-yoga/node'
import * as jwt from "jsonwebtoken";

const prisma = new PrismaClient()
const pubSub = createPubSub()

export type GraphQLContext = {
    prisma: PrismaClient,
    req: any,
    user: number,
    pubSub: PubSub<any>,
}

export function createContext(req: any): GraphQLContext {
    const token = req?.req?.headers['authorization']?.replace('Bearer ', '')
    if (token) {
        const {userId} = jwt.verify(token, process.env.SECRET || 'secretNo') as {userId: string}
        return { prisma, ...req, pubSub, user: userId }
    }
    return { prisma, ...req, pubSub, user: 0}
}
