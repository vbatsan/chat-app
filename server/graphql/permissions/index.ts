import {shield} from "graphql-shield";
import {isAuthenticated} from "./rules";

const permissions = shield({
    Query: {
        users: isAuthenticated,
    },
    Mutation: {
        createRoom: isAuthenticated,
        sendMessage: isAuthenticated,
    },
}, {
    allowExternalErrors: true,
    debug: true,
})

export default permissions;
