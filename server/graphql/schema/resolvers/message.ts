import {Resolvers} from "../../generated/types";
import {GraphQLContext} from "../../../context";
import Message from "../models/Message";
import {SUBSCRIBE_MASSAGE} from "../../constants";

const messageResolver: Resolvers<GraphQLContext> = {
    Query: {
        messages: async (_, {roomId}, {prisma}) => {
            return await prisma.message.findMany({
                where: {
                    roomId
                }
            })
        }
    },
    Mutation: {
        sendMessage: async (_, {message, roomId}, {prisma, user, pubSub}) => {
            const sentMessage =  await prisma.message.create({
                data: {
                    text: message.text,
                    user: {
                        connect: {
                            id: user
                        }
                    },
                    room: {
                        connect: {
                            id: roomId
                        }
                    }
                },
            })

            pubSub.publish(`${SUBSCRIBE_MASSAGE}${roomId}`)
            return sentMessage;
        }
    },
    Subscription: {
        message: {
            subscribe: (_, {roomId}, {pubSub}) => {
                return pubSub.subscribe(`${SUBSCRIBE_MASSAGE}${roomId}`)
            },
            // @ts-ignore
            resolve: async (_, {roomId}, {prisma}) => {
                return await prisma.message.findMany({
                    where: {roomId}
                })
            }
        }
    },
    Message: {
        user: async(_, __, {user, prisma}) => {
            return await prisma.user.findFirst({
                where: {id: _.userId}
            })
        }
    }
}

export default messageResolver;