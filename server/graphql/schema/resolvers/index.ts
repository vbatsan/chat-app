import userResolvers from './user'
import roomResolver from "./room";
import messageResolver from './message'

const resolvers = [
    userResolvers,
    roomResolver,
    messageResolver,
]
export default resolvers
