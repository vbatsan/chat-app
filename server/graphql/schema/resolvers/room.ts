import {Resolvers} from "../../generated/types";
import {GraphQLContext} from "../../../context";
import {generateSubscribeKey} from "../../utils";
import {SUBSCRIBE_ROOM} from "../../constants";

const roomResolver: Resolvers<GraphQLContext> = {
    Query: {
       async rooms(_, {userId}, {prisma}) {
           return await prisma.room.findMany({
                where: {
                   users: {
                       some: {
                           user: {
                               id: userId,
                           }
                       }
                   }
                }
           })
       }
    },
    Mutation: {
        async createRoom(_, {userId}, {user, prisma, pubSub}) {
            const friend = await prisma.user.findFirst({
                where:{
                    id: userId
                }
            })
            const newRoom =  await prisma.room.create({
                data: {
                    name: "Group",
                    users:{
                        create: [
                            {
                                user: {
                                    connect: {
                                        id: user,
                                    }
                                }
                            },
                            {
                                user: {
                                    connect: {
                                        id: userId,
                                    }
                                }
                            }
                        ]
                    }
                }
            })

            pubSub.publish(generateSubscribeKey(SUBSCRIBE_ROOM, user as number))
            pubSub.publish(generateSubscribeKey(SUBSCRIBE_ROOM, userId))
            return newRoom
        }
    },
    Subscription: {
        rooms: {
            subscribe: (_, {userId}, {pubSub}) => {
                pubSub.publish(generateSubscribeKey(SUBSCRIBE_ROOM, userId))
               return  pubSub.subscribe(generateSubscribeKey(SUBSCRIBE_ROOM, userId))
            },
            // @ts-ignore
            resolve: async (_, {userId}, { prisma}) => {
                return await prisma.room.findMany({
                    where: {
                        users: {
                            some: {userId}
                        }
                    }
                })
            }
        }
    },
    Room: {
        name: async (_, __, {user, prisma}) => {
            const roomUsers =  await prisma.user.findMany({
                where: {
                    rooms:{
                        some:{
                            roomId: _.id,
                        }
                    }
                }
            })
            // kostyl :)
            return roomUsers.map(_user => _user.name).join('')
        }
    }
}

export default roomResolver;
