import * as bcrypt from 'bcryptjs'
import * as jwt from 'jsonwebtoken';
import {Resolvers} from "../../generated/types";
import {GraphQLYogaError} from "@graphql-yoga/node";
import {GraphQLContext} from "../../../context";
import {User} from "../models/User";

const userResolver: Resolvers<GraphQLContext> = {
    Query: {

        users: async(_,__, ctx) => {
            return await ctx.prisma.user.findMany()
        },
    },
    Mutation: {
        register: async(_, {name, password}, {prisma}) => {
            const user = await prisma.user.findFirst({
                where: {
                    name
                }
            })
            if (user) {
                throw new GraphQLYogaError('User already exist')
            }
            const hashedPassword = await bcrypt.hash(password, 10)
            return await prisma.user.create({
                data: new User(name, hashedPassword)
            })
        },
        login: async (_, {name, password}, {prisma}) => {
            const user = await prisma.user.findFirst({where: {name}})
            if (!user) {
                throw new GraphQLYogaError('Wrong credentials')
            }
            const valid = await bcrypt.compare(password, user.password)
            if (valid) {
                return {
                    token: jwt.sign({ userId: user.id }, process.env.SECRET || 'secretNo'),
                    user,
                }
            }
            throw new GraphQLYogaError('Wrong credentials')
        },
    },
}
export default userResolver;
