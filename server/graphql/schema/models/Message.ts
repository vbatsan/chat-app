class Message {
    text: string

    constructor(text: string) {
        this.text = text
    }
}

export default Message;