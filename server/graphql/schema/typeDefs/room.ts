const room = `
    type Query {
        rooms(userId: Int!): [Room]!
    }
    
    type Room {
        id: Int!
        name: String
    }
    
    type Mutation {
        createRoom(userId: Int!): Room!
    }
    scalar Date
    
    type Subscription {
        rooms(userId: Int!): [Room]!
    }
`
export default room
