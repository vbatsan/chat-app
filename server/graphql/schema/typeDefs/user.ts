const userTypeDefs = `
    type Query {
        users: [User!]
    }
    type User {
        id: Int!
        name: String!
    }
    type Mutation {
        register(name: String!, password: String!): User!
        login(name: String!, password: String!): LoginResponse!
    }
    type LoginResponse {
        token: String!,
        user: User!
    }
`
export default userTypeDefs


