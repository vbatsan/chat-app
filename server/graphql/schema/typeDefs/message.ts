const message = `
    type Query {
        messages(roomId: Int!): [Message]!
    }
    
    type Mutation {
        sendMessage(roomId: Int!, message: MessageInput!): Message
    }
    
    input MessageInput {
        text: String!
    }
    
     type Message {
        id: Int!
        user: User
        userId: Int!
        text: String!
        date: Date!
    }
    
    type Subscription {
        message(roomId: Int!): [Message]!
    }
`
export default message