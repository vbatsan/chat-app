import userTypeDefs from "./user";
import roomDefs from './room'
import messageDefs from "./message";

const typeDefs = [
    userTypeDefs,
    roomDefs,
    messageDefs,
]

export default typeDefs;
